USE [master]
GO
/****** Object:  Database [JewelleryStore]    Script Date: 01/23/2018 23:19:55 ******/
CREATE DATABASE [JewelleryStore] ON  PRIMARY 
( NAME = N'JewelleryStore', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SUNIL\MSSQL\DATA\JewelleryStore.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'JewelleryStore_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SUNIL\MSSQL\DATA\JewelleryStore_log.LDF' , SIZE = 504KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [JewelleryStore] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [JewelleryStore].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [JewelleryStore] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [JewelleryStore] SET ANSI_NULLS OFF
GO
ALTER DATABASE [JewelleryStore] SET ANSI_PADDING OFF
GO
ALTER DATABASE [JewelleryStore] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [JewelleryStore] SET ARITHABORT OFF
GO
ALTER DATABASE [JewelleryStore] SET AUTO_CLOSE ON
GO
ALTER DATABASE [JewelleryStore] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [JewelleryStore] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [JewelleryStore] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [JewelleryStore] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [JewelleryStore] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [JewelleryStore] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [JewelleryStore] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [JewelleryStore] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [JewelleryStore] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [JewelleryStore] SET  ENABLE_BROKER
GO
ALTER DATABASE [JewelleryStore] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [JewelleryStore] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [JewelleryStore] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [JewelleryStore] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [JewelleryStore] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [JewelleryStore] SET READ_COMMITTED_SNAPSHOT ON
GO
ALTER DATABASE [JewelleryStore] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [JewelleryStore] SET  READ_WRITE
GO
ALTER DATABASE [JewelleryStore] SET RECOVERY SIMPLE
GO
ALTER DATABASE [JewelleryStore] SET  MULTI_USER
GO
ALTER DATABASE [JewelleryStore] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [JewelleryStore] SET DB_CHAINING OFF
GO
USE [JewelleryStore]
GO
/****** Object:  Table [dbo].[ItemMaster]    Script Date: 01/23/2018 23:19:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ItemName] [nvarchar](300) NULL,
	[TaxId] [nvarchar](100) NULL,
	[ItemType] [int] NULL,
 CONSTRAINT [PK_ItemMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ItemMaster] ON
INSERT [dbo].[ItemMaster] ([ID], [ItemName], [TaxId], [ItemType]) VALUES (1, N'My new Item for customers', N'2,3,7', 0)
INSERT [dbo].[ItemMaster] ([ID], [ItemName], [TaxId], [ItemType]) VALUES (4, N'Mary Kom', N'', 0)
INSERT [dbo].[ItemMaster] ([ID], [ItemName], [TaxId], [ItemType]) VALUES (6, N'Pair of Silver Jewellery', N'2,7', 1)
SET IDENTITY_INSERT [dbo].[ItemMaster] OFF
/****** Object:  Table [dbo].[UserDetails]    Script Date: 01/23/2018 23:19:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.UserDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UserDetails] ON
INSERT [dbo].[UserDetails] ([ID], [UserName], [Password], [CreatedDate], [IsActive], [IsAdmin]) VALUES (1, N'AppStudio', N'appstudio99', CAST(0x0000A83700000000 AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[UserDetails] OFF
/****** Object:  Table [dbo].[Taxes]    Script Date: 01/23/2018 23:19:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Taxes](
	[TaxID] [int] IDENTITY(1,1) NOT NULL,
	[TaxName] [nvarchar](max) NULL,
	[TaxValue] [decimal](18, 2) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.Taxes] PRIMARY KEY CLUSTERED 
(
	[TaxID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Taxes] ON
INSERT [dbo].[Taxes] ([TaxID], [TaxName], [TaxValue], [IsActive], [CreatedOn]) VALUES (2, N'Tax', CAST(2.78 AS Decimal(18, 2)), 1, CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[Taxes] ([TaxID], [TaxName], [TaxValue], [IsActive], [CreatedOn]) VALUES (3, N'SGST', CAST(12.00 AS Decimal(18, 2)), 1, CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[Taxes] ([TaxID], [TaxName], [TaxValue], [IsActive], [CreatedOn]) VALUES (7, N'ghjg', CAST(6.00 AS Decimal(18, 2)), 1, CAST(0x0000000000000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[Taxes] OFF
/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 01/23/2018 23:19:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitString]
(    
      @Input NVARCHAR(MAX),
      @Character CHAR(1)
)
RETURNS @Output TABLE (
      Item NVARCHAR(1000)
)
AS
BEGIN
      DECLARE @StartIndex INT, @EndIndex INT
 
      SET @StartIndex = 1
      IF SUBSTRING(@Input, LEN(@Input) - 1, LEN(@Input)) <> @Character
      BEGIN
            SET @Input = @Input + @Character
      END
 
      WHILE CHARINDEX(@Character, @Input) > 0
      BEGIN
            SET @EndIndex = CHARINDEX(@Character, @Input)
           
            INSERT INTO @Output(Item)
            SELECT SUBSTRING(@Input, @StartIndex, @EndIndex - 1)
           
            SET @Input = SUBSTRING(@Input, @EndIndex + 1, LEN(@Input))
      END
 
      RETURN
END
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 01/23/2018 23:19:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201711252033204_InitialCreate', N'DataAccess.JewelleryContext', 0x1F8B0800000000000400CD59CD72DB3610BE77A6EFC0E1A99D7104FF5C5C0F958C23D91DB591ED099DDC217225630A022C003AD2B3E5D047EA2B74C17F41B424DA91DDD18504B0DF2E763FEC62A97FBFFF137C5826DC7B04A5991443FF6470EC7B20221933B118FA9999BF3BF73FBCFFF9A7E02A4E96DED76ADD995D8792420FFD0763D20B4274F40009D58384454A6A393783482684C6929C1E1FFF464E4E0820848F589E177CCE846109E42FF83A922282D464944F650C5C97E33813E6A8DE0D4D40A73482A13FA6865E461168ED7B979C51B420043EF73D2A8434D4A07D175F34844649B108531CA0FC7E9502AE9B53AEA1B4FBA259BEEF168E4FED164823584145993632E909787256FA84B8E2CFF2AC5FFB0CBD7685DE352BBBEBDC7343FF9E2E7DCFD57331E2CAAE69FB74908B32D0031439F29A89A33AF8C811FB3BF24619379982A180CC28CA8FBCBB6CC659F427ACEEE55F208622E3BC6D161A86736B033874A7640ACAAC3EC3BC317632F63DB22E4A5CD95A725DACD8D14498B353DFBB4113E88C431DFBD6EE432315FC0E02143510DF51634061E82631E4DEDB3060539D7DA81422DFF0C8F8DE942E3F81589887A13FB54EBF664B88AB91D2882F82E1094321A332D843CF57CAB35AD118229650EE7B770A9FCA637BEE7B6144ED3EBB36BD5DC1445F46863DD60A3E4AC9818ADE382305D691B7A2B6145FEF9975D156A4803474DD4A623CD56A0C8632AEFB92B925FA46A47E0EA35F93CED643AFC2E73BAAF537A9E2832B2AF96859D89391873B238813274CF483D97E40B07622AF31ECA58A3FE01B700E2A9F80A5E9382A18EBF2B4E8D295EB4617C0219876F5686C280AEE201F25DB252DAB3A44D74E72D756EB4D353701525C05AA2B0379E2CE104C699A22A15A778872C40B8B0BC4E85DD8BFC2260506897447A1ADADAD35E159A40B706651355A7ACD94363605CDA80DF4284E36966D84F0092757EA9A28B979A8F17CB5D63E17EB5B79B243B4F1DB356E25C12492EF0A6AE59D0A73395B89A8EAAECE23C9B3446CA9F33B308A14E5A01483BD70CA92EA0095A3FB233579A18DD48CEE8FD4AA9E6DA8D6F02656409C10B9E1271BF1772A904BA5BD88961FEA6732AD7DF2FB336EABF493317268D78F734D5D6C6334A3FB233585AF8DD48CF6E64A51D93AD8524CBC058BEBFAE6001583AFCCE08D62E22EA9B5D745C5291E4199C87777A51B99BD58626FE9F291C536AB872B6D2019D80583F06F3EE20CF7DB2C9852C1E6A04D71CDF4B1F09C3B0DEEFFA7D9245AC77C77C7F946BD1FB36EDD791DEE797373DA3DF14855F440D52F095DFEDA067B4E4B171FBAA59B31F3F2762EC65773A076EED51BAA8370C4EDA15E4412B74F7A1158472FB467380F432EA70FDA0DD1AF07DABC673FBBC129F238BA6B26D1DAC23E9C00BD57EF533739DB5BA02E253FA247DA2C5F01697F7A0DC6A0D9A281B01F620544B62E34A0D59A8998CB2A8CB8CBB645D51227CA53343FB6773F65D89C4606A7ED25306FF7F30438F4AF9219C413719B993433975A4332E36BDF2A02B25D7FDE08AEDB1CDCA6F64DFF882DA099CC9E945BF131633CAEEDBEEEA0E81310968C659241AB426393CD625523DD48B12750E9BE31A4206C8ABA8724E508A66F4548ED79EC6F1B92EC132C68B4AA6E214F83EC0EC4BADB8331A30B45135D6234F2F6EF0462FF4F78FF1F46D1449581180000, N'6.2.0-61023')
/****** Object:  Table [dbo].[Company]    Script Date: 01/23/2018 23:19:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](200) NOT NULL,
	[CompanyAddress] [nvarchar](300) NOT NULL,
	[CompanyEmail] [nvarchar](100) NULL,
	[Phone] [nvarchar](100) NULL,
	[Mobile] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
	[Facebook] [nvarchar](100) NULL,
	[GooglePlus] [nvarchar](100) NULL,
	[Twitter] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[GstNo] [nvarchar](100) NULL,
	[TinNo] [nvarchar](100) NULL,
	[Promoters] [nvarchar](100) NULL,
	[Logo] [nvarchar](100) NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Company] ON
INSERT [dbo].[Company] ([CompanyID], [CompanyName], [CompanyAddress], [CompanyEmail], [Phone], [Mobile], [Website], [Facebook], [GooglePlus], [Twitter], [CreatedDate], [GstNo], [TinNo], [Promoters], [Logo]) VALUES (1, N'AppStudio99', N'#121', N'appstudio99@gmail.com', N'73073007968', N'979879798', N'www.google.com', N'www.facebook.com', N'www.googleplus.com', N'www.twitter.com', CAST(0x0000A85300D9F294 AS DateTime), N'GST-1278681', N'TIN- 11627627', N'AppStudio99', N'')
SET IDENTITY_INSERT [dbo].[Company] OFF
/****** Object:  StoredProcedure [dbo].[AddItems]    Script Date: 01/23/2018 23:19:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AddItems](
@itemname nvarchar(200),
@taxid nvarchar(100),
@type int,
@itemID int=0,
@opcode int) as
begin
if(@opcode=1)
begin
insert into ItemMaster(ItemName,TaxID,ItemType) values (@itemname,@taxid,@type)
end

if(@opcode=2)
begin
update ItemMaster set ItemName=@itemname,taxid=@taxid,ItemType=@type where ID=@itemID
end

end
GO
/****** Object:  StoredProcedure [dbo].[spCompany]    Script Date: 01/23/2018 23:19:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spCompany]
@CompanyID int,
@CompanyName nvarchar(200),
@CompanyAddress nvarchar(300),
@CompanyEmail nvarchar(100),
@Phone nvarchar(100),
@Mobile nvarchar(100),
@Website nvarchar(100),
@Facebook nvarchar(100),
@GooglePlus nvarchar(100),
@Twitter nvarchar(100),
@CreatedDate datetime,
@GstNo nvarchar(100),
@TinNo nvarchar(100),
@Promotors nvarchar(100),
@Logo nvarchar(100),
@Opcode int
as 
begin
if(@Opcode=1)
 begin
  insert into Company (CompanyAddress,CompanyEmail,CompanyName,CreatedDate,Facebook,GooglePlus,GstNo,Logo,Mobile,Phone,Promoters,TinNo,Twitter,Website)
   values
  (@CompanyAddress,@CompanyEmail,@CompanyName,@CreatedDate,@Facebook,@GooglePlus,@GstNo,@Logo,@Mobile,@Phone,@Promotors,@TinNo,@Twitter,@Website)
 end
 
 if(@Opcode=2)
	begin
		update Company set CompanyAddress=@CompanyAddress,CompanyEmail=@CompanyEmail, CompanyName=@CompanyName, Facebook=@Facebook,GooglePlus=@GooglePlus,GstNo=@GstNo,
		Logo=@Logo,Mobile=@Mobile,Phone=@Phone,Promoters=@Promotors,TinNo=@TinNo, Twitter=@Twitter,Website=@Website
		where CompanyID=@CompanyID
	end
end
GO
/****** Object:  StoredProcedure [dbo].[GetListings]    Script Date: 01/23/2018 23:19:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetListings]
@opcode int
as
begin
if(@opcode=1)
begin
DECLARE @TStudent nvarchar(100) 
 select m.ID,m.ItemName,t1.TaxName, 
 CASE  
  WHEN m.itemtype=0 THEN 'Gold' 
  ELSE 'Silver' 
END as itemType from  ItemMaster m, Taxes t1
 where t1.TaxID in(
   SELECT Item
FROM dbo.SplitString(m.TaxID, ',')) order by m.ID
end
end
GO
