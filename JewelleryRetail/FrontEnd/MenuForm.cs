﻿using JewelleryRetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JewelleryRetail.FrontEnd
{
    public partial class MenuForm : FormHandler
    {
        public MenuForm()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rect = panel1.ClientRectangle;
            rect.Width--;
            rect.Height--;
            e.Graphics.DrawRectangle(Pens.White, rect);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
            Tax tax = new Tax();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);                   
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
            ItemListMaster tax = new ItemListMaster();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Close();
            ItemListMaster tax = new ItemListMaster();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            CompanyDetails tax = new CompanyDetails();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Close();
            Billing tax = new Billing();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }
    }
}
