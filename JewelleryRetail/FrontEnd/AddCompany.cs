﻿using JewelleryRetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JewelleryRetail.FrontEnd
{
    public partial class AddCompany : FormHandler
    {
        public AddCompany()
        {
            InitializeComponent();
        }

        private int CompanyID { get; set; }
        public AddCompany(int id)
        {
            InitializeComponent();
            CompanyID = id;
            using (SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                DataTable dt = new DataTable();
                SqlCommand Cmd = new SqlCommand("select * from Company where CompanyID='" + id + "'");
                Cmd.Connection = Con;
                if (Con.State == ConnectionState.Closed)
                    Con.Open();
                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                adp.Fill(dt);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.txtItenName.Text = Convert.ToString(dt.Rows[0]["CompanyName"]);
                    this.textBox1.Text = Convert.ToString(dt.Rows[0]["CompanyEmail"]);
                    this.textBox2.Text = Convert.ToString(dt.Rows[0]["Phone"]);
                    this.textBox3.Text = Convert.ToString(dt.Rows[0]["Mobile"]);
                    this.textBox4.Text = Convert.ToString(dt.Rows[0]["CompanyAddress"]);
                    this.textBox5.Text = Convert.ToString(dt.Rows[0]["Promoters"]);
                    this.textBox12.Text = Convert.ToString(dt.Rows[0]["Logo"]);
                    this.textBox11.Text = Convert.ToString(dt.Rows[0]["GstNo"]);
                    this.textBox10.Text = Convert.ToString(dt.Rows[0]["TinNo"]);
                    this.textBox9.Text = Convert.ToString(dt.Rows[0]["Website"]);
                    this.textBox8.Text = Convert.ToString(dt.Rows[0]["Facebook"]);
                    this.textBox7.Text = Convert.ToString(dt.Rows[0]["GooglePlus"]);
                    this.textBox6.Text = Convert.ToString(dt.Rows[0]["Twitter"]);
                    btnCancel.Text = "Modify";
                }
            }

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
            CompanyDetails tax = new CompanyDetails();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            SqlCommand Cmd = new SqlCommand();
            using (SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                Cmd.CommandText = "spCompany";
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Connection = Con;
                if (Con.State == ConnectionState.Closed)
                    Con.Open();
                if (btnCancel.Text == "Modify")
                {
                    Cmd.Parameters.AddWithValue("@Opcode", 2);
                    Cmd.Parameters.AddWithValue("@CompanyID", CompanyID);
                }
                else
                {
                    Cmd.Parameters.AddWithValue("@Opcode", 1);
                    Cmd.Parameters.AddWithValue("@CompanyID", 0);
                }
                Cmd.Parameters.AddWithValue("@CompanyName", txtItenName.Text.Trim());
                Cmd.Parameters.AddWithValue("@CompanyEmail", textBox1.Text.Trim());
                Cmd.Parameters.AddWithValue("@Phone", textBox2.Text.Trim());
                Cmd.Parameters.AddWithValue("@Mobile", textBox3.Text.Trim());
                Cmd.Parameters.AddWithValue("@CompanyAddress", textBox4.Text.Trim());
                Cmd.Parameters.AddWithValue("@Promotors", textBox5.Text.Trim());
                Cmd.Parameters.AddWithValue("@Logo", textBox12.Text.Trim());
                Cmd.Parameters.AddWithValue("@GstNo", textBox11.Text.Trim());
                Cmd.Parameters.AddWithValue("@TinNo", textBox10.Text.Trim());
                Cmd.Parameters.AddWithValue("@Website", textBox9.Text.Trim());
                Cmd.Parameters.AddWithValue("@Facebook", textBox8.Text.Trim());
                Cmd.Parameters.AddWithValue("@GooglePlus", textBox7.Text.Trim());
                Cmd.Parameters.AddWithValue("@Twitter", textBox6.Text.Trim());
                Cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                Cmd.ExecuteNonQuery();
            }
            Cmd.Dispose();
            MessageBox.Show("Data Inserted Successfully", "AppStudio99 Message");
            EmptyTextBoxes(this.Controls);
        }
        public void EmptyTextBoxes(Control.ControlCollection parent)
        {
            foreach (Control c in parent.OfType<Panel>())
            {
                foreach (Control cd in c.Controls)
                {
                    if (cd.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(cd)).Text = string.Empty;
                    }
                }
            }

        }
    }
}
