﻿using JewelleryRetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JewelleryRetail.FrontEnd
{
    public partial class CompanyDetails : FormHandler
    {
        public CompanyDetails()
        {
            InitializeComponent();
        }
        private void CompanyDetails_Load(object sender, EventArgs e)
        {
            FillGrid();
        }

        public void FillGrid()
        {
            using (SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                DataTable dt = new DataTable();
                SqlCommand Cmd = new SqlCommand();
                Cmd.CommandText = "select * from Company";
                Cmd.CommandType = CommandType.Text;
                Cmd.Connection = Con;
                if (Con.State == ConnectionState.Closed)
                    Con.Open();
                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                adp.Fill(dt);
                if (dt != null && dt.Rows.Count > 0)
                {
                    button1.Visible = false;
                    DataView view = new DataView(dt);
                    dataGridView1.Rows.Add(dt.Rows.Count);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dataGridView1.Rows[i].Cells[0].Value = Convert.ToString(dt.Rows[i]["CompanyName"]);
                        dataGridView1.Rows[i].Cells[1].Value = Convert.ToString(dt.Rows[i]["CompanyEmail"]);
                        dataGridView1.Rows[i].Cells[2].Value = Convert.ToString(dt.Rows[i]["Phone"]);
                        dataGridView1.Rows[i].Cells[3].Value = Convert.ToString(dt.Rows[i]["Mobile"]);
                        dataGridView1.Rows[i].Cells[4].Value = Convert.ToString(dt.Rows[i]["Website"]);
                        dataGridView1.Rows[i].Cells[5].Value = Convert.ToString(dt.Rows[i]["GstNo"]);
                        dataGridView1.Rows[i].Cells[6].Value = Convert.ToString(dt.Rows[i]["TinNo"]);
                        dataGridView1.Rows[i].Cells[7].Value = Convert.ToString(dt.Rows[i]["CompanyAddress"]);
                        dataGridView1.Rows[i].Cells[8].Value = Convert.ToString(dt.Rows[i]["CreatedDate"]);
                        dataGridView1.Rows[i].Cells[9].Value = Convert.ToString(dt.Rows[i]["CompanyID"]);
                    }
                    dataGridView1.Refresh();
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rect = panel1.ClientRectangle;
            rect.Width--;
            rect.Height--;
            e.Graphics.DrawRectangle(Pens.White, rect);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            AddCompany tax = new AddCompany();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
            MenuForm tax = new MenuForm();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (dgv == null)
                return;
            if (dgv.CurrentRow.Selected)
            {
                int CompanyID = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[9].Value);
                if (CompanyID > 0)
                {
                    this.Close();
                    AddCompany frm = new AddCompany(CompanyID);
                    frm.StartPosition = FormStartPosition.CenterScreen;
                    frm.Show(Application.OpenForms["MainForm"]);
                }
            }
        }
    }
}
