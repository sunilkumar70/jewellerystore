﻿using JewelleryRetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JewelleryRetail.FrontEnd
{
    public partial class ItemListMaster : FormHandler
    {
        public ItemListMaster()
        {
            InitializeComponent();
            this.TopMost = false;
        }

        private void ItemListMaster_Load(object sender, EventArgs e)
        {
            using (SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                DataTable dt = new DataTable();

                SqlCommand Cmd = new SqlCommand();
                Cmd.CommandText = "GetListings";
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Connection = Con;
                if (Con.State == ConnectionState.Closed)
                    Con.Open();
                Cmd.Parameters.AddWithValue("@opcode", 1);
                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                adp.Fill(dt);
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataView view = new DataView(dt);
                    DataTable distinctValues = view.ToTable(true, "ID");
                    dataGridView1.Rows.Add(dt.Rows.Count);
                    int oldvalue = -1,cc=1 ;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (oldvalue != Convert.ToInt32(dt.Rows[i][0]))
                        {
                            dataGridView1.Rows[i].Cells[0].Value = Convert.ToString(dt.Rows[i][0]);
                            dataGridView1.Rows[i].Cells[1].Value = Convert.ToString(dt.Rows[i][1]);
                            dataGridView1.Rows[i].Cells[2].Value = Convert.ToString(dt.Rows[i][2]);
                            dataGridView1.Rows[i].Cells[3].Value = Convert.ToString(dt.Rows[i][3]);
                            oldvalue = Convert.ToInt32(dt.Rows[i][0]);
                            cc = 1;
                        }
                        else
                        {                            
                            dataGridView1.Rows[i - cc].Cells[2].Value += "," + Convert.ToString(dt.Rows[i][2]);
                            cc++;
                            oldvalue = Convert.ToInt32(dt.Rows[i][0]);
                        }

                    }
                }
                dataGridView1.Refresh();
                dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                for (int i = dataGridView1.Rows.Count - 1; i > -1; --i)
                {
                    DataGridViewRow row = dataGridView1.Rows[i];
                    if (!row.IsNewRow && row.Cells[0].Value == null)
                    {
                        dataGridView1.Rows.RemoveAt(i);
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
            MenuForm tax = new MenuForm();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            ItemsForm tax = new ItemsForm();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                try
                {
                    foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                    {
                        var id = row.Cells[0].Value;
                        using (SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
                        {
                            DataTable dt = new DataTable();
                            SqlCommand Cmd = new SqlCommand("delete from ItemMaster where ID='" + id + "'");
                            Cmd.Connection = Con;
                            if (Con.State == ConnectionState.Closed)
                                Con.Open();

                            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                            adp.Fill(dt);
                        }
                        dataGridView1.Rows.RemoveAt(row.Index);
                    }
                    MessageBox.Show("Records deleted successfully", "AppStudio99 Message");
                }
                catch (Exception ex) { }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.SelectedRows.Count < 2)
            {
                var id = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
                this.Close();
                ItemsForm frm = new ItemsForm(id);
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.Show(Application.OpenForms["MainForm"]);

            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
