﻿
using JewelleryRetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JewelleryRetail.FrontEnd
{
    public partial class LoginForm : FormHandler
    {
        public LoginForm()
        {
            InitializeComponent();
        }
    
        private void LoginForm_Load(object sender, EventArgs e)
        {
            txtUserName.Focus();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var username = txtUserName.Text.ToLower().Trim();
            var password = txtPassword.Text.ToLower().Trim();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                string query = "select * from userdetails where lower(username)='"+username+"' and lower(password)='"+password+"'";
                SqlDataAdapter adp = new SqlDataAdapter(query, con);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if(dt!=null && dt.Rows.Count > 0)
                {
                    MessageBox.Show("Login Successfull");
                    this.Close();
                    MenuForm tax = new MenuForm();
                    tax.StartPosition = FormStartPosition.CenterScreen;
                    tax.Show(Application.OpenForms["MainForm"]);
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
