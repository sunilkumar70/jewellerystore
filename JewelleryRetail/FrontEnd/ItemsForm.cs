﻿using JewelleryRetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JewelleryRetail.FrontEnd
{
    public partial class ItemsForm : FormHandler
    {
        public ItemsForm()
        {
           
            InitializeComponent();
        }
        public int ItemID = 0;
        public ItemsForm(int id)
        {
            InitializeComponent();
            FillCheckList();
            ItemID = id;
            Dictionary<string, string> comboSource = new Dictionary<string, string>();
            comboSource.Add("1", "Silver");
            comboSource.Add("0", "Gold");
            comboBox1.DataSource = new BindingSource(comboSource, null);
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
            using (SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                DataTable dt = new DataTable();
                SqlCommand Cmd = new SqlCommand("select * from ItemMaster where ID='" + id + "'");
                Cmd.Connection = Con;
                if (Con.State == ConnectionState.Closed)
                    Con.Open();
                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                adp.Fill(dt);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.txtItenName.Text = Convert.ToString(dt.Rows[0]["ItemName"]);
                    var tax = Convert.ToString(dt.Rows[0]["TaxId"]).Split(',');
                    var type = Convert.ToInt32(dt.Rows[0]["ItemType"]);
                    this.comboBox1.SelectedItem = type == 1 ? "Silver" : "Gold";
                    this.comboBox1.SelectedText = type == 1 ? "Silver" : "Gold";
                    this.comboBox1.SelectedIndex = type;
                    this.comboBox1.Refresh();
                    if (tax != null)
                    {
                        foreach (var item in tax)
                        {
                            var oid = Convert.ToInt32(item);
                            var items = this.checkedListBox1.Items;
                            for (int i = 0; i < items.Count; i++)
                            {
                                DataRowView castedItem = this.checkedListBox1.Items[i] as DataRowView;
                                var sid = Convert.ToInt32(castedItem[0]);
                                if (sid == oid)
                                {
                                    this.checkedListBox1.SetItemChecked(i, true);
                                }
                            }
                        }
                        btnSave.Text = "Modify";
                    }
                }

            }
        }
        private void ItemsForm_Load(object sender, EventArgs e)
        {
            Dictionary<string, string> comboSource = new Dictionary<string, string>();
            comboSource.Add("1", "Silver");
            comboSource.Add("0", "Gold");
            comboBox1.DataSource = new BindingSource(comboSource, null);
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
            FillCheckList();
            comboBox1.SelectedIndex = 0;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
            ItemListMaster tax = new ItemListMaster();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        public void FillCheckList()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                string query = "select TaxID , (TaxName +' - '+CONVERT(varchar(10), TaxValue)) as TaxName from taxes";
                SqlDataAdapter adp = new SqlDataAdapter(query, con);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                ((ListBox)checkedListBox1).DataSource = dt;
                ((ListBox)checkedListBox1).DisplayMember = "TaxName";
                ((ListBox)checkedListBox1).ValueMember = "TaxID";
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var name = txtItenName.Text.Trim();
            var type = comboBox1.SelectedIndex;
            var tax = checkedListBox1.SelectedItems.Count;
            if (tax > 0)
            {
                string taxid = "";
                List<string> list = new List<string>();
                foreach (object itemChecked in checkedListBox1.CheckedItems)
                {
                    DataRowView castedItem = itemChecked as DataRowView;
                    var id = castedItem[0];
                    list.Add(id.ToString());
                }
                taxid = string.Join(",", list);
                SqlCommand Cmd = new SqlCommand();
                using (SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
                {
                    Cmd.CommandText = "AddItems";
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Connection = Con;
                    if (Con.State == ConnectionState.Closed)
                        Con.Open();
                    if (btnSave.Text == "Modify")
                    {
                        Cmd.Parameters.AddWithValue("@opcode", 2);
                    }
                    else
                    {
                        Cmd.Parameters.AddWithValue("@opcode", 1);
                    }
                    Cmd.Parameters.AddWithValue("@itemname", name);
                    Cmd.Parameters.AddWithValue("@type", type);
                    Cmd.Parameters.AddWithValue("@taxid", taxid);
                    Cmd.Parameters.AddWithValue("@itemID", ItemID);
                    Cmd.ExecuteNonQuery();
                }
                Cmd.Dispose();
                MessageBox.Show("Data Inserted Successfully", "AppStudio99 Message");
                txtItenName.Text = "";
            }
        }


    }
}
