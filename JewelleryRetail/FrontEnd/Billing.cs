﻿using JewelleryRetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JewelleryRetail.FrontEnd
{
    public partial class Billing : FormHandler
    {
        public Billing()
        {
            InitializeComponent();
        }

        private void Billing_Load(object sender, EventArgs e)
        {
            FillGrid();
        }

        private DataTable dt = new DataTable();
        public void FillGrid()
        {           
          
            DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();          
            cmb.Name = "cmb";
            cmb.HeaderText = "Item Type";
            cmb.MaxDropDownItems = 2;
            cmb.Items.Add("Gold");
            cmb.Items.Add("Silver");
            cmb.DisplayMember = "Gold";
            dataGridView1.Columns.Add(cmb);

            DataGridViewTextBoxColumn cmb1 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn cmb2 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn cmb3 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn cmb4 = new DataGridViewTextBoxColumn();
            cmb1.Name = "cmb1";
            cmb1.HeaderText = "Particulars";
            cmb1.Width = 500;
            dataGridView1.Columns.Add(cmb1);

            cmb2.Name = "cmb2";
            cmb2.HeaderText = "Weight";
            cmb2.Width = 100;
            dataGridView1.Columns.Add(cmb2);
            cmb3.Name = "cmb3";
            cmb3.HeaderText = "Labour";
            cmb3.Width = 100;
            dataGridView1.Columns.Add(cmb3);
            cmb4.Name = "cmb4";
            cmb4.HeaderText = "Amount";
            cmb4.Width = 100;
            dataGridView1.Columns.Add(cmb4);
            dataGridView1.Rows.Add(10);
            for (int i = 0; i< dataGridView1.Rows.Count; i++){
                dataGridView1.Rows[i].Cells[0].Value = "Gold";
            }
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            string titleText = dataGridView1.Columns[1].HeaderText;
            if (titleText.Equals("Particulars"))
            {
                TextBox autoText = e.Control as TextBox;
                if (autoText != null)
                {
                    autoText.AutoCompleteMode = AutoCompleteMode.Suggest;
                    autoText.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    AutoCompleteStringCollection DataCollection = new AutoCompleteStringCollection();
                    addItems(DataCollection);
                    autoText.AutoCompleteCustomSource = DataCollection;
                }
            }
        }
        public void addItems(AutoCompleteStringCollection col)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                string query = "select * from ItemMaster";
                SqlDataAdapter adp = new SqlDataAdapter(query, con);
                dt = new DataTable();
                adp.Fill(dt);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        col.Add(Convert.ToString(item["ItemName"])+"-"+Convert.ToString(item["ID"]));                        
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
            MenuForm tax = new MenuForm();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                var colValue = Convert.ToString(dataGridView1[e.ColumnIndex, e.RowIndex].Value);
                if (!string.IsNullOrEmpty(colValue))
                {
                    SendKeys.Send("{up}");
                    SendKeys.Send("{right}");
                }
            }
        }
    }
}
