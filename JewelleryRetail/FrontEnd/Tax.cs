﻿using JewelleryRetail.FrontEnd;
using JewelleryRetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JewelleryRetail.FrontEnd
{
    public partial class Tax : FormHandler
    {
        public Tax()
        {
            InitializeComponent();
            this.TopMost = false;
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rect = panel1.ClientRectangle;
            rect.Width--;
            rect.Height--;
            e.Graphics.DrawRectangle(Pens.White, rect);
        }
        public int TaxID = 0;
        private void btnSave_Click(object sender, EventArgs e)
        {
            var name = txtTaxName.Text.Trim();
            var value = txtValue.Text.Trim();
            var date = DateTime.Now.Date.ToShortDateString();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                string query = "insert into Taxes (CreatedOn,IsActive,TaxName,TaxValue) values (" + date + ",'" + true + "','" + name + "','" + Convert.ToDecimal(value) + "')";
                if (btnSave.Text=="Modify")
                {
                     query = "update Taxes set TaxName='"+name+"',TaxValue='"+value+"' where TaxID='"+TaxID+"'";
                }                
                SqlDataAdapter adp = new SqlDataAdapter(query, con);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                MessageBox.Show("Data Saved Successfully");
                if (btnSave.Text == "Modify")
                {
                    btnSave.Text = "Save";
                }
                FillGrid();
                txtTaxName.Text = "";
                txtValue.Text = "";
            }

        }

        public void FillGrid()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
            {
                string query = "select TaxID as #, TaxName as Tax, TaxValue as Value from taxes";
                SqlDataAdapter adp = new SqlDataAdapter(query, con);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                dgvResult.DataSource = dt;
                dgvResult.Refresh();
            }

        }

        private void Tax_Load(object sender, EventArgs e)
        {
            dgvResult.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            FillGrid();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
            MenuForm tax = new MenuForm();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.Show(Application.OpenForms["MainForm"]);
        }

        private void Tax_SizeChanged(object sender, EventArgs e)
        {
            //minimizeAll();
        }
        int flag = 0;
        public void minimizeAll()
        {
            if (flag <= 0)
            {
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.WindowState == FormWindowState.Maximized || frm.WindowState == FormWindowState.Normal)
                    {
                        frm.WindowState = FormWindowState.Minimized;
                    }
                    flag++;
                }
            }
            else
            {
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.Name == "MainForm")
                    {
                        frm.WindowState = FormWindowState.Maximized;
                    }
                    else
                    {
                        frm.WindowState = FormWindowState.Normal;
                    }
                    flag--;
                }
            }
        }

        private void dgvResult_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (dgv == null)
                return;
            if (dgv.CurrentRow.Selected)
            {
                TaxID = Convert.ToInt32(dgvResult.SelectedRows[0].Cells[0].Value);
                var text = Convert.ToString(dgvResult.SelectedRows[0].Cells[1].Value);
                var value = Convert.ToString(dgvResult.SelectedRows[0].Cells[2].Value);
                txtTaxName.Text = text;
                txtValue.Text = value;
                btnSave.Text = "Modify";
            }
        }
    }
}
