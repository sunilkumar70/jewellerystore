﻿using JewelleryRetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JewelleryRetail.FrontEnd
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.TopMost = false;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            notifyIcon1.BalloonTipText = "Application Minimized.";
            notifyIcon1.BalloonTipTitle = "Jewellery Store";

            LoginForm tax = new LoginForm();
            tax.StartPosition = FormStartPosition.CenterScreen;
            tax.TopMost = true;
            tax.Show(this);
        }
        int flag = 0;
        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
          // minimizeAll();
        }
        public void minimizeAll()
        {
            if (flag <= 0)
            {
                foreach (Form frm in Application.OpenForms)
                {                  
                    if (frm.WindowState == FormWindowState.Maximized || frm.WindowState == FormWindowState.Normal)
                    {
                        frm.WindowState = FormWindowState.Minimized;
                        notifyIcon1.Visible = true;
                        notifyIcon1.ShowBalloonTip(1000);
                    }
                    flag++;
                }
               
            }
            else
            {
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.Name == "MainForm")
                    {
                        frm.WindowState = FormWindowState.Maximized;
                    }
                    else
                    {
                        frm.WindowState = FormWindowState.Normal;
                    }
                    flag--;
                }                
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            minimizeAll();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            notifyIcon1.Visible = false;
        }
    }
}
