﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JewelleryRetail.Models
{
    public class FormHandler:Form
    {
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FormHandler
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Name = "FormHandler";
            this.Load += new System.EventHandler(this.FormHandler_Load);
            this.ResumeLayout(false);
           
        }

        private void FormHandler_Load(object sender, EventArgs e)
        {
            
        }
    }
}
