﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JewelleryRetail.Models
{
    public class TaxModel
    {
        public int TaxID { get; set; }
        public string Tax { get; set; }
        public string Value { get; set; }
        
        public TaxModel() { }
        
           
    }
}
